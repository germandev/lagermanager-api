const express = require('express');
const router = express.Router();

router.get('/getCategories', (req, res) =>
{
	const catData = req.app.get('catData');
	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify(catData));
});

module.exports = router;