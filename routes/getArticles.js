const express = require('express');
const router = express.Router();


router.get('/getArticles', (req, res) =>
{
	const artData = req.app.get('artData');
	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify(artData));
});

module.exports = router;