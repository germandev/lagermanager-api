const express = require('express');
const router = express.Router();


router.get('/getArticle/:type?/:value?', (req, res) =>
{
	res.setHeader('Content-Type', 'application/json');
	if(!req.params.type)
		return res.send('{ "Error": { "Message": "Missing \'Type\'-Parameter" } }');
	if(!req.params.value)
		return res.send('{ "Error": { "Message": "Missing \'Value\'-Parameter" } }');
	const artData = req.app.get('artData');
	let givenID = -1;
	switch(req.params.type.toLowerCase())
	{
		case 'id':
			givenID = parseInt(req.params.value);
			if(!Number.isInteger(givenID))
				return res.send('{ "Error": { "Message": "Given \'ID\'-Parameter is not a number" } }');

			if(givenID > artData.length + 1)
				return res.send('{ "Error": { "Message": "Item with given ID does not exist" } }');

			if(givenID < 1)
				return res.send('{ "Error": { "Message": "Item ID can\'t negative or null" } }');

			return res.send(JSON.stringify(artData[givenID - 1]));


		case 'ean':
			for(let i = 0; i < artData.length; i++)
			{
				if(artData[i].EAN == req.params.value)
					return res.send(JSON.stringify(artData[i]));
			}
			return res.send('{ "Error": { "Message": "Item with given EAN does not exist" } }');

		case 'sku':
			for(let i = 0; i < artData.length; i++)
			{
				if(artData[i].SKU == req.params.value)
					return res.send(JSON.stringify(artData[i]));
			}
			return res.send('{ "Error": { "Message": "Item with given EAN does not exist" } }');

		case 'name':
			const tmpDataArray = [];
			for(let i = 0; i < artData.length; i++)
			{
				if(artData[i].Name.toLowerCase().match(req.params.value.toLowerCase()))
					tmpDataArray.push(artData[i]);
			}
			return res.send(JSON.stringify(tmpDataArray));

		default:
			return res.send('{ "Error": { "Message": "Wrong \'Type\'-Parameter" } }');
	}
});

module.exports = router;