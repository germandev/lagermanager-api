const express = require('express');
const app = express();
const logger = require('./util/Logger');
const mysql = require('mysql2/promise');
const sqlConfig = require('./config/sql.js');

logger.log('Setting application port');
app.set('port', process.env.PORT || 3000);
logger.log(`Port set to ${app.get('port')}`);

logger.log('Setting SQL Handle');
logger.log('Connecting to SQL Server');

async function main()
{
	let connection = null;
	try
	{
		connection = await mysql.createConnection(sqlConfig);
	}
	catch (err)
	{
		logger.error(err);
		process.exit(-1);
	}
	let artData = null;
	let catData = null;
	let supData = null;
	let warData = null;
	let usrData = null;

	try
	{
		logger.log('Fetching database');
		const artrows = await connection.execute('SELECT * FROM article');
		const catrows = await connection.execute('SELECT * FROM category');
		const suprows = await connection.execute('SELECT * FROM supplier');
		const warrows = await connection.execute('SELECT * FROM warehouse');
		const accrows = await connection.execute('SELECT ID, Firstname, Lastname, Mail FROM account');

		logger.log('Filling vars');

		usrData = accrows[0];

		warData = warrows[0];
		for(let i = 0; i < warData.length; i++)
			warData[i].CreatedFrom = (usrData[warData[i].CreatedFrom - 1]) ? usrData[warData[i].CreatedFrom - 1] : warData[i].CreatedFrom;

		supData = suprows[0];
		for(let i = 0; i < supData.length; i++)
			supData[i].CreatedFrom = (usrData[supData[i].CreatedFrom - 1]) ? usrData[supData[i].CreatedFrom - 1] : supData[i].CreatedFrom;

		catData = catrows[0];
		for(let i = 0; i < catData.length; i++)
			catData[i].CreatedFrom = (usrData[catData[i].CreatedFrom - 1]) ? usrData[catData[i].CreatedFrom - 1] : catData[i].CreatedFrom;

		artData = artrows[0];
		for(let i = 0; i < artData.length; i++)
		{
			artData[i].Category = (catData[artData[i].Category - 1]) ? catData[artData[i].Category - 1] : artData[i].Category;
			artData[i].Supplier = (supData[artData[i].Supplier - 1]) ? supData[artData[i].Supplier - 1] : artData[i].Supplier;
			artData[i].Warehouse = (warData[artData[i].Warehouse - 1]) ? warData[artData[i].Warehouse - 1] : artData[i].Warehouse;
		}

		logger.log('Startup SQL finished');
	}
	catch(err)
	{
		logger.error(err);
		process.exit(-1);
	}

	logger.log('Setting App-Vars');
	app.set('artData', artData);
	app.set('catData', catData);
	app.set('supData', supData);
	app.set('warData', warData);
	app.set('usrData', usrData);
	app.set('loggerHandle', logger);

	logger.log('Setting up routes');
	app.use(require('./routes/getArticles'));
	app.use(require('./routes/getArticle'));
	app.use(require('./routes/getCategories'));

	logger.log(`Trying to listen on port ${app.get('port')}`);
	app.listen(app.get('port'), () =>
	{
		logger.log(`Listening on port ${app.get('port')}`);
	}).on('error', (err) =>
	{
		logger.error(err);
		process.exit(-1);
	});
}

main();